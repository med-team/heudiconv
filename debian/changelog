heudiconv (1.3.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 13 Nov 2024 13:26:08 -0500

heudiconv (1.1.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.1.6
  * update dependencies:
    - python3-mock
    - python3-six
    + python3-boto3
    + python3-duecredit
    + python3-inotify
    + python3-tinydb

 -- Alexandre Detiste <tchet@debian.org>  Fri, 10 May 2024 12:55:16 +0200

heudiconv (0.13.1-1) experimental; urgency=medium

  * Recent upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 30 May 2023 14:32:31 -0400

heudiconv (0.11.6-1) unstable; urgency=medium

  * Fresh upstream release: primarily documentation fixes

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 21 Nov 2022 15:49:26 -0500

heudiconv (0.11.4-1) unstable; urgency=medium

  * Fresh upstream release
    - fixes some tests which can fail with buggy datalad (Closes: #1015102)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 29 Sep 2022 19:12:48 -0400

heudiconv (0.11.3-1) unstable; urgency=medium

  * Fresh upstream release
  * CPed d/patches/0001* dropped, the other one refreshed
  * d/watch - switch to download "proper" source distribution from pypi
    instead of github.  github would not have heudicon/_version.py
    (coming with 0.11.2) produced by PEP 518 versioningit workflow.

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 12 May 2022 20:37:06 -0400

heudiconv (0.10.0-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 0.10.0
  * d/rules: copy test dir to build directory before
    tests, since some files are not copied by default
  * Drop merged patches, pull a upstream patch to fix tests
  * Add python3:Depends to Depends field, use --with python3 to
    use the python helper
  * Set PYTHONPATH to properly generate manpage
  * Fix copyright years
  * Migrate to secure URI

 -- Nilesh Patra <nilesh@debian.org>  Wed, 17 Nov 2021 22:52:11 +0530

heudiconv (0.9.0-3) unstable; urgency=medium

  * Team upload.
  * remove debian/gbp.conf
  * Move package to Debian Med team
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Do not parse d/changelog (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * watch file standard 4 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 17 Nov 2021 10:42:36 +0100

heudiconv (0.9.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix autopkgtests
  * d/watch: Fix broken regex in watch file
  * Pull upstream patch to fix compatibility with new datalad

 -- Nilesh Patra <nilesh@debian.org>  Sat, 13 Nov 2021 11:23:03 +0000

heudiconv (0.9.0-2) unstable; urgency=medium

  * Replace python with python3 as a Depends (Closes: #982120)
  * Suppress warnings from python while producing manpages to make build
    reproducible.  Thanks Chris Lamb (Closes: #923170)
  * debian/patches
    + 0001-BF-datalad-use-public-call_git-added-in-0.12.0-not-d.patch
      for compatibility with datalad 0.14 which deprecated a protected method

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 06 Feb 2021 12:24:25 -0500

heudiconv (0.9.0-1) unstable; urgency=medium

  * Fresh upstream release, another attempt at unstable (needs python3-nipype,
    in NEW queue)
  * debian/control
    - boost to debcompat 10
    - switch to use python3- versions of packages
      (Closes #934687)
    - require nipype >= 1.2.3 as the one fixing handling of DWI files

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 23 Dec 2020 10:27:58 -0500

heudiconv (0.6.0-1) UNRELEASED; urgency=medium

  * Fresh upstream release, upload to unstable
  * debian/rules
    - cleanup artifacts from running tests at build time
  * debian/control
    - python-filelock into (B)Depends

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 05 Dec 2019 21:18:16 -0500

heudiconv (0.5.4-1) experimental; urgency=medium

  * Fresh upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 10 May 2019 13:05:22 -0400

heudiconv (0.5.3-1) unstable; urgency=medium

  * Fresh minor bugfix release

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 12 Jan 2019 09:54:18 -0500

heudiconv (0.5.2-1) unstable; urgency=medium

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 04 Jan 2019 10:35:47 -0500

heudiconv (0.5.1-1) neurodebian; urgency=medium

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 12 Nov 2018 09:00:13 -0500

heudiconv (0.5-1) neurodebian; urgency=medium

  * Fresh upstream release
    - huge refactoring to provide a heudiconv module
    - explicit assignment of the license (apache-2.0)
  * debian/
    - depend on python-pathlib and dcm2niix (from Recommends)
    - remove recommends of mricron, no longer used
    - python-nipype and dcm2niix into build-depends for build-time testing
    - custom patch for trusty to tollerate exec in setup.py

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 01 Mar 2018 18:39:07 -0500

heudiconv (0.4-1) neurodebian; urgency=medium

  * Recent upstream release
    - lots of changes
  * debian/control
    - Vcs fields
    - myself to uploaders
    - uses pytest for tests
    - added python-datalad to Recommends
    - added python-dicom and python-dcmstack to B-Depends for tests
  * debian/copyright
    - adjusted years, source url
  * debian/tests
    - added for rudimentary autopkgtest

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 07 Dec 2017 20:14:38 -0500

heudiconv (0.1+git94-g85a2afb-1) neurodebian; urgency=low

  * New upstream snapshot.
    - More heuristics.
    - More DICOM converters supported (e.g. dcm2niix)
  * Install generated manpage.
  * Switch to pybuild from custom Makefile.

 -- Michael Hanke <mih@debian.org>  Tue, 18 Apr 2017 08:31:40 +0200

heudiconv (0.1-1) neurodebian; urgency=low

  * Initial release.

 -- Michael Hanke <mih@debian.org>  Fri, 04 Sep 2015 18:08:10 +0200
